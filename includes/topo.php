


<div class="container-fluid">
  <div class="row topo">

<div class="container top20">
  <div class="row">

    <div class="col-xs-2 logo">
      <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
      </a>
    </div>



    <div class="col-xs-10 padding0">
      <!-- ======================================================================= -->
      <!-- ENDERECO E TELEFONES    -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 padding0 top25">
        <div class="telefone_topo pull-right">
          <p>
            <img class="right10" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone_topo.png" alt="" >
            <span><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>

            <b class="left15"></b>

            <?php if (!empty($config[telefone2])) { ?>
                <img class="right10" src="<?php echo Util::caminho_projeto();?>/imgs/icon_whats.png" alt=""><span>
                <?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?>
              <?php } ?>


            </p>

          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- ENDERECO E TELEFONES    -->
        <!-- ======================================================================= -->



        <!--  ==============================================================  -->
        <!--CARRINHO-->
        <!--  ==============================================================  -->
        <div class="col-xs-4 carrinho text-right">
          <div class="dropdown">
            <a class="btn btn_topo" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/btn_ocamento.png" alt="" />
              <span class="badge"><?php echo /*count($_SESSION[solicitacoes_produtos])+*/count($_SESSION[solicitacoes_servicos]); ?></span>
            </a>

            <div class="dropdown-menu topo-meu-orcamento " aria-labelledby="dropdownMenu1">

              <?php /*
              if(count($_SESSION[solicitacoes_produtos]) > 0){
                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                  $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                  ?>
                  <tr>
                    <td>
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 98, 65, array("class"=>"img-rounded", "alt"=>"$row[titulo]")) ?>
                    </td>
                    <td class="col-xs-12"><?php Util::imprime($row[titulo]); ?></td>
                    <td class="text-center">
                      QTD.<br>
                      <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                      <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                    </td>
                    <td class="text-center">
                      <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <i class="fa fa-times-circle fa-2x top20"></i>
                      </a>
                    </td>
                  </tr>
                  <?php
                }
              }
              */?>

              <!--  ==============================================================  -->
              <!--sessao adicionar servicos-->
              <!--  ==============================================================  -->
              <?php
              if(count($_SESSION[solicitacoes_servicos]) > 0)
              {
                for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                  ?>
                  <div class="lista-itens-carrinho">
                    <div class="col-xs-2">
                      <?php if(!empty($row[imagem])): ?>
                        <img class="carrinho_servcos" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                        <!-- <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?> -->
                      <?php endif; ?>
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1">
                      <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                    </div>
                    <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>

                  </div>
                  <?php
                }
              }
              ?>

              <div class="col-xs-12 text-right top10 bottom20">
                <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn_orcamento" >
                  ENVIAR ORÇAMENTO
                </a>
              </div>
            </div>
          </div>
        </div>
        <!--  ==============================================================  -->
        <!--CARRINHO-->
        <!--  ==============================================================  -->

        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->
        <div class="col-xs-12 menu">
            <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
            <div id="navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li >
                        <a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                           href="<?php echo Util::caminho_projeto() ?>/">HOME
                        </a>
                    </li>

                    <li >
                        <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                           href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA
                        </a>
                    </li>


                    <li>
                        <a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico" or Url::getURL( 0 ) == "orcamento" ){ echo "active"; } ?>"
                           href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
                        </a>
                    </li>

                    <li >
                        <a class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>"
                           href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS
                        </a>
                    </li>

                    <li >
                        <a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
                           href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO
                        </a>
                    </li>

                    <li >
                        <a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
                           href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO
                        </a>
                    </li>

                </ul>

            </div><!--/.nav-collapse -->

        </div>
        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->

      </div>



     </div>
   </div>
  </div>
</div>
