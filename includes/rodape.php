<div class="top50"></div>
<div class="container-fluid rodape">
	<div class="row">

		<a href="#" class="scrollup">scrollup</a>


		<div class="container top20 bottom20">
			<div class="row">

				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->
				<div class="col-xs-9">
					<div class="barra_branca">
						<ul class="menu_rodape">
							<li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
							</li>
							<li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a>
							</li>


							<li class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a>
							</li>

							<li  class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica" ){ echo "active"; } ?>">
								<a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a>
							</li>


							<li  class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>">
								<a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
							</li>

							<li class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
							</li>
						</ul>

					</div>
				</div>
				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->


				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->
				<div class="col-xs-3 redes text-right top20">
					<?php if ($config[google_plus] != "") { ?>
						<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
							<i class="fa fa-google-plus-official fa-2x right15"></i>
						</a>
						<?php } ?>

						<?php if ($config[facebook] != "") { ?>
							<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
								<i class="fa fa-facebook-official fa-2x right15"></i>
							</a>
							<?php } ?>

							<?php if ($config[instagram] != "") { ?>
								<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
									<i class="fa fa-instagram fa-2x right15"></i>
								</a>
								<?php } ?>

							</div>
							<!-- ======================================================================= -->
							<!-- redes sociais    -->
							<!-- ======================================================================= -->


							<div class="clearfix">	</div>


							<!-- ======================================================================= -->
							<!-- LOGO    -->
							<!-- ======================================================================= -->
							<div class="col-xs-3 top15">
								<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
									<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" />
								</a>
							</div>
							<!-- ======================================================================= -->
							<!-- LOGO    -->
							<!-- ======================================================================= -->

							<!-- ======================================================================= -->
							<!-- ENDERECO E TELEFONES    -->
							<!-- ======================================================================= -->
							<div class="col-xs-7 padding0 top10 endereco_rodape">
								<div class="telefone_rodape">
									<p class="bottom15">
										<i class="fa fa-phone right10"></i>
										<span><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>

										<b class="left15"></b>

										<?php if (!empty($config[telefone2])) { ?>
											<i class="fa fa-whatsapp right10"></i>
											<span><?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?>
											<?php } ?>

											<?php if (!empty($config[telefone3])) { ?>
												<i class="fa fa-phone right10"></i>
												<span><?php Util::imprime($config[ddd3]); ?></span><?php Util::imprime($config[telefone3]); ?>
												<?php } ?>

												<?php if (!empty($config[telefone4])) { ?>
													<i class="fa fa-phone right10"></i>
													<span><?php Util::imprime($config[ddd4]); ?></span><?php Util::imprime($config[telefone4]); ?>
													<?php } ?>
												</p>

											</div>
											<p class="bottom15"><i class="fa fa-map-marker right10"></i><?php Util::imprime($config[endereco]); ?></p>

											<div class="col-xs-8">
												<p>Formas de Pagamento</p>
													<img class="input100" src="<?php echo Util::caminho_projeto(); ?>/imgs/forma-pagamento.png" alt="">
											</div>


										</div>
										<!-- ======================================================================= -->
										<!-- ENDERECO E TELEFONES    -->
										<!-- ======================================================================= -->


										<div class="col-xs-2 padding0 text-right top45">

											<a href="http://www.homewebbrasil.com.br" target="_blank">
												<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
											</a>
										</div>





									</div>
								</div>
							</div>
						</div>

						<div class="container-fluid">
							<div class="row rodape-preto">
								<div class="col-xs-12 text-center top10 bottom10">
									<h5>© Copyright GRUPO PARIS.</h5>
								</div>
							</div>
						</div>


						<?php if(!empty($config[ddd2]) and !empty($config[telefone2]) ): ?>                      
	<div class="whatsappFixed">
		<a href="https://api.whatsapp.com/send?phone=55<?php echo Util::trata_numero_whatsapp($config[ddd2].$config[telefone2]); ?>&text=Olá,%20gostaria%20de%20solicitar%20um%20orçamento.">
			<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/whatsapp.png" alt="">
		</a>
	</div>         
<?php endif; ?>