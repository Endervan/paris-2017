
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/dicas/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!doctype html>
<html amp lang="pt-br">
    <head>
        <?php require_once("../includes/head.php"); ?>

        <style amp-custom>
            <?php require_once("../css/geral.css"); ?>
            <?php require_once("../css/topo_rodape.css"); ?>
            <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11); ?>
        .bg-interna{
        	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
					background-size: 100% 12.94rem /* 207/16 */;
        }
        </style>
				<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
				<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
				<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>


    </head>





<body class="bg-interna">


	<?php
	$voltar_para = 'dicas'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php")
	?>


      <div class="row">
          <div class="col-12 localizacao-pagina-dicas localizacao-pagina text-center top35">
              <h3 class="text-uppercase"><?php echo Util::imprime($dados_dentro[titulo]) ?></h3>
              <amp-img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/borda_titulo_dentro.jpg" alt="Home"

              height="5"
							width="90">
              </amp-img>

          </div>

      </div>

      <div class="row">
          <div class="col-12 bottom50">
              <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($dados_dentro[imagem]) ?>"
								on="tap:lightbox2"
								role="button"
								tabindex="0"
              alt="Home" height="260" width="460">
              </amp-img>
              <p class="top10"><?php echo Util::imprime($dados_dentro[descricao]) ?></p>
          </div>
					<amp-image-lightbox id="lightbox2"layout="nodisplay">	</amp-image-lightbox>
      </div>


      <?php require_once("../includes/rodape.php") ?>

  </body>



</html>
