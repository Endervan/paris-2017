<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
    background-size: 100% 12.94rem /* 207/16 */;
  }
  </style>


  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>

</head>





<body class="bg-interna">


  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once("../includes/topo.php")
  ?>


  <div class="row">
    <div class="col-12 localizacao-pagina-fale top50 localizacao-pagina">
      <h1>TRABALHE CONOSCO</h1>
      <p>Faça parte da nossa equipe, envie um currículo</p>
    </div>

    <!-- ======================================================================= -->
    <!-- ATENDIMENTO HOME -->
    <!-- ======================================================================= -->
    <div class="col-7 atendimento_contatos text-right top10 pg0">

      <h2>
        <span class="right10"> <?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>
      </h2>

    </div>

    <div class="col-5">
      <a class="btn btn_ligue input100"href="tel:+55<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>">
        LIGUE AGORA
      </a>
    </div>

    <!-- ======================================================================= -->
    <!-- ATENDIMENTO HOME -->
    <!-- ======================================================================= -->


  </div>



  <div class="row bottom50">

    <div class="col-12 top20">




          <?php

          if(!empty($_FILES[curriculo][name])):
            $nome_arquivo = Util::upload_arquivo("../../uploads", $_FILES[curriculo]);
            $texto = "Anexo: ";
            $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
            $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
          endif;

		//  VERIFICO SE E PARA ENVIAR O EMAIL
		if(isset($_POST[nome])){
                  				$texto_mensagem = "
                          Nome: ".($_POST[nome])." <br />
                          Email: ".($_POST[email])." <br />
                          Sexo: ".($_POST[sexo])." <br />
                          Nascimento: ".($_POST[nascimento])." <br />
                          Grau de Instrução: ".($_POST[grau_instrucao])." <br />
                          Endereco: ".($_POST[endereco])." <br />
                          Area Predentido: ".($_POST[area_predentido])." <br />
                          Cargo Predentido: ".($_POST[cargo_predentido])." <br />
                          Curricúlo: ".($_POST[curriculo])." <br />

                  				Mensagem: <br />
                  				".(nl2br($_POST[mensagem]))."
                  				";

			if(Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email])){
                Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
                $enviado = 'sim';
                unset($_POST);
            }




		}
		?>


        <?php if($enviado == 'sim'): ?>
            <div class="col-12  text-center top40">
                <h1>Email enviado com sucesso.</h1>
            </div>
        <?php else: ?>




          <form method="post" class="p2" action-xhr="index.php" target="_top">


        <div class="ampstart-input inline-block relative m0 p0 mb3">

          <div class="relativo">
            <div class="">NOME:</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="nome"  required>
            <span class="fa fa-user form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div class="">EMAIL:</div>
            <input type="email" class="input-form input100 block border-none p0 m0" name="email"  required>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div class="">TELEFONE</div>
            <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone"  required>
            <span class="fa fa-phone form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div class="">SEXO</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="sexo"  required>
            <span class="fa fa-star form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div class="">NASCIMENTO</div>
            <input class="input-form input100 block border-none p0 m0" name="nascimento"  type="date" required>
            <span class="fa fa-calendar form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div>GRAU DE INSTRUÇÃO</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="grau_instrucao" required>
            <span class="fa fa-list-alt form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div>ENDEREÇO</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="endereco" required>
            <span class="fa fa-map-marker form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div>ÁREA PRETENDIDO</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="area_predentido" required>
            <span class="fa fa-folder-open form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div>CARGO PRETENDIDO</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="cargo_predentido" placeholder="" required>
            <span class="fa fa-folder-open form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div>CURRÍCULO</div>
            <input type="file" class="input-form input100 block border-none p0 m0 pt20 curriculo" name="curriculo" required>
            <span class="fa fa-file-text form-control-feedback"></span>
          </div>


          <div class="relativo">
            <div>MENSAGEM</div>
            <textarea name="mensagem" class="input-form input100 campo-textarea" ></textarea>
            <span class="fa fa-pencil form-control-feedback"></span>
          </div>

        </div>

        <div class="col-12  text-right padding0">
          <div class="relativo ">
              <button type="submit"
                  value="OK"
                  class="btn btn-primary">ENVIAR</button>
          </div>
        </div>


        <div submit-success>
          <template type="amp-mustache">
            Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
          </template>
        </div>

        <div submit-error>
          <template type="amp-mustache">
            Houve um erro, {{name}} por favor tente novamente.
          </template>
        </div>


      </form>
    <?php endif; ?>

    </div>

  </div>


  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
