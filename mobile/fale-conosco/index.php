<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>

  <?php require_once("../includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>


              form.amp-form-submit-success [submit-success],
              form.amp-form-submit-error [submit-error]{
                margin-top: 1rem /* 16/16 */;
              }
              form.amp-form-submit-success [submit-success] {
                color: green;
              }
              form.amp-form-submit-error [submit-error] {
                color: red;
              }
              form.amp-form-submit-success.hide-inputs > input {
                display: none;
              }



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
    background-size: 100% 12.94rem /* 207/16 */;
  }
  </style>


  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>

</head>





<body class="bg-interna">


  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once("../includes/topo.php")
  ?>


  <div class="row">
    <div class="col-12 localizacao-pagina-fale top50 localizacao-pagina">
      <h1>FALE CONOSCO</h1>
      <p>Envie suas dúvidas, sugestões, elogios ou críticas.</p>
    </div>

    <div class="clearfix"></div>


    <!-- ======================================================================= -->
    <!-- ATENDIMENTO HOME -->
    <!-- ======================================================================= -->
    <div class="col-7 atendimento_contatos text-right top10 pg0">

      <h2>
        <span class="right10"> <?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>
      </h2>

    </div>

    <div class="col-5">
      <a class="btn btn_ligue input100"href="tel:+55<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>">
        LIGUE AGORA
      </a>
    </div>

    <!-- ======================================================================= -->
    <!-- ATENDIMENTO HOME -->
    <!-- ======================================================================= -->



  </div>



  <div class="row bottom50 top20">

    <div class="col-12">



                        <?php
            			//  VERIFICO SE E PARA ENVIAR O EMAIL
            			if(isset($_POST[nome])){
            				$texto_mensagem = "
                                				Nome: ".($_POST[nome])." <br />
                                				Email: ".($_POST[email])." <br />
                                				Telefone: ".($_POST[telefone])." <br />
                                				Assunto: ".($_POST[assunto])." <br />
                                				Fala com: ".($_POST[fala])." <br />

                                				Mensagem: <br />
                                				".(nl2br($_POST[mensagem]))."
                                				";

            				if(Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email])){
                              Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
                              $enviado = 'sim';
                              unset($_POST);
                          }




            			}
            			?>


                      <?php if($enviado == 'sim'): ?>
                          <div class="col-12  text-center top40">
                              <h1>Email enviado com sucesso.</h1>
                          </div>
                      <?php else: ?>



          <form method="post" class="p2" action-xhr="index.php" target="_top">


        <div class="ampstart-input inline-block relative form_contatos m0 p0 mb3">
          <div class="relativo">
            <input type="text" class="input-form input100 block border-none p0 m0" name="nome" placeholder="NOME" required>
            <span class="fa fa-user form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="email" class="input-form input100 block border-none p0 m0" name="email" placeholder="EMAIL" required>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone" placeholder="TELEFONE" required>
            <span class="fa fa-phone form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="text" class="input-form input100 block border-none p0 m0" name="assunto" placeholder="ASSUNTO" required>
            <span class="fa fa-star form-control-feedback"></span>
          </div>

          <div class="relativo">
            <textarea name="mensagem" placeholder="MENSAGEM" class="input-form input100 campo-textarea" ></textarea>
            <span class="fa fa-pencil form-control-feedback"></span>
          </div>

        </div>

        <div class="col-12  text-right padding0">
          <div class="relativo ">
              <button type="submit"
                  value="OK"
                  class="btn btn-primary">ENVIAR</button>
          </div>
        </div>


        <div submit-success>
          <template type="amp-mustache">
            Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
          </template>
        </div>

        <div submit-error>
          <template type="amp-mustache">
            Houve um erro, {{name}} por favor tente novamente.
          </template>
        </div>


      </form>
    <?php endif; ?>

    </div>

  </div>


  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
