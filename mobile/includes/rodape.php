<div class="row">
    <div class="separador-rodape clearfix"></div>
    <div class="rodape col-12">
        <div class="col-6">
            <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
                <i class="fa fa-phone" aria-hidden="true"></i> LIGAR AGORA
            </a>
        </div>
        <div class="col-6 text-right">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento">
                MEU ORÇAMENTO <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </a>  
        </div>
    </div>
</div>



<div class="whatsappFixed">
    <a href="https://api.whatsapp.com/send?phone=55<?php echo Util::trata_numero_whatsapp($config[ddd2].$config[telefone2]); ?>&text=Olá,%20gostaria%20de%20solicitar%20um%20orçamento.">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/whatsapp.png" alt="">
    </a>
</div>