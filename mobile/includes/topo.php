<div class="row">
    <div class="col-3">
        <?php
        if(empty($voltar_para)){
            $link_topo = Util::caminho_projeto()."/mobile/";
        }else{
            $link_topo = Util::caminho_projeto()."/mobile/".$voltar_para;
        }
        ?>
        <a href="<?php echo $link_topo  ?>"><i class="fa fa-arrow-left fa-2x top15 btn-topo" aria-hidden="true"></i></a>
    </div>
    <div class="col-6 topo top15">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile">

        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo-paginas.png" alt="Home" height="34" width="181"></amp-img>

      </a>
    </div>
    <div class="col-3 text-right">
        <button on="tap:sidebar.toggle" class="ampstart-btn caps m2 btn-topo top15"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></button>
    </div>
</div>



<amp-sidebar id="sidebar" layout="nodisplay" side="left" class="menu-mobile-principal">
  <ul class="menu-mobile">
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile"> <i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa"> <i class="fa fa-building-o" aria-hidden="true"></i> Empresa</a></li>
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos"> <i class="fa fa-suitcase" aria-hidden="true"></i> Servicos</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas"> <i class="fa fa-circle" aria-hidden="true"></i> Dicas</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco"> <i class="fa fa-envelope" aria-hidden="true"></i> Fale Conosco</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco"> <i class="fa fa-group" aria-hidden="true"></i> Trabalhe Conosco</a></li>
  </ul>
</amp-sidebar>
