<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];



function adiciona($id, $tipo_orcamento){
  //  VERIFICO SE O TIPO DE SOLICITACAO E DE SERVICO OU PRODUTO
  switch($tipo_orcamento){
    case "servico":
    //	VERIFICO SE NAO TEM JA ADICIONADO
    if(count($_SESSION[solicitacoes_servicos] > 0)):
      if (!@in_array($id, $_SESSION[solicitacoes_servicos])) :
        $_SESSION[solicitacoes_servicos][] = $id;
      endif;
    else:
      $_SESSION[solicitacoes_servicos][] = $id;
    endif;
    break;
    // case "produto":
    //     //	VERIFICO SE NAO TEM JA ADICIONADO
    //     if(count($_SESSION[solicitacoes_produtos] > 0)):
    //             if (!@in_array($id, $_SESSION[solicitacoes_produtos])) :
    //                     $_SESSION[solicitacoes_produtos][] = $id;
    //             endif;
    //     else:
    //             $_SESSION[solicitacoes_produtos][] = $id;
    //     endif;
    // break;
  }
}



function excluir($id, $tipo_orcamento){
  switch ($tipo_orcamento) {
    // case 'produto':
    //     unset($_SESSION[solicitacoes_produtos][$id]);
    //     sort($_SESSION[solicitacoes_produtos]);
    // break;
    case 'servico';
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
  }
}




//  EXCLUI OU ADD UM ITEM
if($_GET[action] != ''){
  //  SELECIONO O TIPO
  switch($_GET[action]){
    case "add":
    adiciona($_GET[id], $_GET[tipo_orcamento]);
    break;
    case "del":
    excluir($_GET[id], $_GET[tipo_orcamento]);
    break;
  }
}


?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
    background-size: 100% 12.94rem /* 207/16 */;
  }
  </style>




</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>


  <div class="row">
    <div class="col-12 localizacao-pagina-orcamento localizacao-pagina">
      <p>ENVIE SEU</p>
      <h1>ORÇAMENTO</h1>
    </div>
  </div>






<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_GET[nome])){
  //  CADASTRO OS PRODUTOS SOLICITADOS
  for($i=0; $i < count($_GET[qtd]); $i++){
    $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_GET[idproduto][$i]);
    $produtos .= "
                    <tr>
                    <td><p>". $_GET[qtd][$i] ."</p></td>
                    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                    </tr>
                    ";
  }

  //  CADASTRO OS SERVICOS SOLICITADOS
  for($i=0; $i < count($_GET[qtd_servico]); $i++){
    $dados = $obj_site->select_unico("tb_servicos", "idservico", $_GET[idservico][$i]);
    $produtos .= "
                    <tr>
                    <td><p>". $_GET[qtd_servico][$i] ."</p></td>
                    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                    </tr>
                    ";
  }


  //  ENVIANDO A MENSAGEM PARA O CLIENTE
   $texto_mensagem = "
                      O seguinte cliente fez uma solicitação pelo site. <br />

                      Assunto: ".($_GET[assunto])." <br />
                      Nome: ".($_GET[nome])." <br />
                      Email: ".($_GET[email])." <br />
                      Telefone: ".($_GET[telefone])." <br />
                      Celular: ".($_GET[celular])." <br />

                      Mensagem: <br />
                      ". nl2br($_GET[mensagem]) ." <br />


                      <h2> Produtos selecionados:</h2> <br />
                      <table width='100%' border='0' cellpadding='5' cellspacing='5'>
                      <tr>
                      <td><h4>QTD</h4></td>
                      <td><h4>PRODUTO</h4></td>
                      </tr>
                      $produtos
                      </table>
                      ";


  if (Util::envia_email($config[email],("$_GET[nome] solicitou um orçamento"),($texto_mensagem),($_GET[nome]), $_GET[email])) {
      Util::envia_email($config[email_copia],("$_GET[nome] solicitou um orçamento"),($texto_mensagem),($_GET[nome]), $_GET[email]);
      unset($_SESSION[solicitacoes_produtos]);
      unset($_SESSION[solicitacoes_servicos]);
      $enviado = 'sim';
  }




}
?>





 <?php if ($enviado == 'sim'): ?>
      <div class="col-12  text-center top40">
          <h1>Orçamento enviado com sucesso.</h1>
          <a class="btn btn_2 top15" href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">
              VER MAIS SERVIÇOS
          </a>
      </div>
  <?php else: ?>
   




  <div class="row">
    <div class="col-12">
      <form method="get" action="index.php" target="_top">

        <?php require_once('../includes/lista_itens_orcamento.php'); ?>


        <div class="ampstart-input inline-block relative m0 p0 mb3 orcamento-input">
          <input type="text" class="input-form input100 block border-none p0 m0" name="nome" placeholder="NOME" required>
          <input type="email" class="input-form input100 block border-none p0 m0" name="email" placeholder="EMAIL" required>
          <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone" placeholder="TELEFONE" required>
          <input type="text" class="input-form input100 block border-none p0 m0" name="localidade" placeholder="LOCALIDADE" required>
          <textarea name="mensagem" placeholder="MENSAGEM" class="input-form input100 campo-textarea" ></textarea>
        </div>
        <div class="text-right">
          <input type="submit" value="ENVIAR" class="ampstart-btn caps btn btn-primary">
        </div>


        <div submit-success>
          <template type="amp-mustache">
            Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
          </template>
        </div>

        <div submit-error>
          <template type="amp-mustache">
            Houve um erro, {{name}} por favor tente novamente.
          </template>
        </div>


      </form>
    </div>
  </div>
  <?php endif; ?>




	<div class="top80"></div>


  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
