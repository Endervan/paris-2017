
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("./includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("./css/geral.css"); ?>
  <?php require_once("./css/topo_rodape.css"); ?>
  <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  </style>
</head>



<body class="bg-index">


  <div class="row">
    <div class="col-12 text-center topo">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" height="100" width="150"></amp-img>
    </div>
  </div>


  <div class="row font-index">
    <div class="col-12">
      <div class="text-center">
        <?php //$row = $obj_site->select_unico("tb_empresa", "idempresa", 11);?>
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>
    </div>


    <div class="col-4 top10 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">
        <div class="index-bg-icones">
          <amp-img src="./imgs/icon_empresa_home.png" alt="A EMPRESA" height="37" width="37"></amp-img>
        </div>
        <h1>A EMPRESA</h1>
      </a>
    </div>

    <div class="col-4 top10 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">
        <div class="index-bg-icones">
          <amp-img src="./imgs/icon_servicos_home.png" alt="PRODUTOS" height="37" width="37"></amp-img>
        </div>
        <h1>SERVIÇOS</h1>
      </a>
    </div>
    <div class="col-4 top10 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas">
        <div class="index-bg-icones">
          <amp-img src="./imgs/icon_dicas_home.png" alt="DICAS" height="37" width="37"></amp-img>
        </div>
        <h1>DICAS</h1>
      </a>
    </div>



    <div class="col-4 col-offset-2 top20 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">
        <div class="index-bg-icones">
          <amp-img src="./imgs/icon_fale_home.png" alt="FALE CONOSCO" height="37" width="37"></amp-img>
        </div>
        <h1>FALE CONOSCO</h1>
      </a>
    </div>


    <div class="col-4 top20 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/onde-estamos">
        <div class="index-bg-icones">
          <i class="fa fa-map-marker" aria-hidden="true" style="color: #000; font-size: 2.5em;"></i>
        </div>
        <h1>ONDE ESTAMOS</h1>
      </a>
    </div>



  </div>


  <?php require_once("./includes/rodape.php") ?>


</body>



</html>
