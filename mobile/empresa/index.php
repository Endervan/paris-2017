<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
    background-size: 100% 12.94rem /* 207/16 */;
  }
  </style>


  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>


</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">


    <div class="col-12 top40 localizacao-pagina">
      <h2>CONHEÇA MAIS O </h2>
      <h1>GRUPO PARIS</h1>
      <p>COMUNICAÇÃO VISUAL E IMPRESSÃO DIGITAL</p>
    </div>


    <div class="col-12 desc-empresa">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 10);?>
      <p><?php Util::imprime($row[descricao]); ?></p>
    </div>



    <!-- ======================================================================= -->
    <!-- MISSAO  -->
    <!-- ======================================================================= -->
    <div class="col-12 top20">
      <div class="bg_amarelo pg10 text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 12);?>
        <div class="top15">
          <h3><?php Util::imprime($row[titulo]); ?></h3>
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_home.png" height="8" width="135"></amp-img>
        </div>

        <div class="top25 pt20">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- MISSAO  -->
    <!-- ======================================================================= -->

    <!-- ======================================================================= -->
    <!-- VALORES  -->
    <!-- ======================================================================= -->
    <div class="col-12 top10">
      <div class="bg_amarelo pg10 text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 13);?>
        <div class="top15">
          <h3><?php Util::imprime($row[titulo]); ?></h3>
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_home.png" height="8" width="135"></amp-img>
        </div>

        <div class="top25 pt20">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- VALORES  -->
    <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!-- VISAO  -->
    <!-- ======================================================================= -->
    <div class="col-xs-12 top10">
      <div class="bg_amarelo pg10 text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 14);?>
        <div class="top15">
          <h3> <?php Util::imprime($row[titulo]); ?></h3>
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_home.png" height="8" width="135"></amp-img>
        </div>

        <div class="top25 pt20">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- VISAO  -->
    <!-- ======================================================================= -->

  </div>



  <div class="row">
    <div class="col-12 top10 clientes_emp bottom50">

      <h2 class="bottom50">NOSSOS CLIENTES</h2>

      <amp-carousel controls class = "carrossel-preview"
      type="slides"
      width="480"
      height="150"
      layout="responsive"
      [slide]="selectedSlide"
      on="slideChange:AMP.setState({selectedSlide: event.index})">


      <?php
      $i = 0;
      $result = $obj_site->select("tb_clientes");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
          ?>


            <amp-img
            src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
            layout="responsive"
            width="400"
            height="100">
          </amp-img>
        <?php
      }
      $i++;
    }
    ?>
  </amp-carousel>


</div>
</div>


<?php require_once("../includes/rodape.php") ?>

</body>



</html>
