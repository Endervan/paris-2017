<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
    background-size: 100% 12.94rem /* 207/16 */;
  }
  </style>

</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>


  <div class="col-12 localizacao-pagina-dicas localizacao-pagina top25">
    <h2>CONFIRA AS NOSSOS</h2>
    <h1>SERVIÇOS</h1>
  </div>




  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->
  <div class="row">

    <div class="col-12 text-center">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 15);?>
    </div>
    <div class="col-12 servicos_desc">
      <p><?php Util::imprime($row[descricao]); ?></p>
    </div>


    <div class="top50 servicos_home">
      <?php
      $result = $obj_site->select("tb_servicos");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
          //  busca a qtd de produtos cadastrados
          // $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


          switch ($i) {
            case 1:
            $cor_class = 'FACHADA_EM_LONA';
            break;
            case 2:
            $cor_class = 'CARTOES_VISITAS';
            break;
            case 3:
            $cor_class = 'BANNER_EM_CAVALE';
            break;
            case 4:
            $cor_class = 'FAIXAS_TECIDOS';
            break;
            case 5:
            $cor_class = 'EMPRESSAO_EM_TECIDOS';
            break;

            default:
            $cor_class = 'WIND_FANG';
            break;
          }
          $i++;
          ?>
          <!-- ======================================================================= -->
          <!--ITEM 01 REPETI ATE 6 ITENS   -->
          <!-- ======================================================================= -->

          <div class="col-6 text-center">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <div class="circle">
                <amp-img  layout="responsive" class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" height="50" width="50" alt="<?php echo Util::imprime($row[titulo]) ?>"></amp-img>
                <div class="col-12  servicos_titulo text-center"><h1 ><?php Util::imprime($row[titulo]); ?></h1></div>
              </div>
            </a>
          </div>



          <?php
        }
      }
      ?>


    </div>

  </div>
  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->





  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
