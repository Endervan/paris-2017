
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/servicos/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!doctype html>
<html amp lang="pt-br">
<head>
	<?php require_once("../includes/head.php"); ?>

	<style amp-custom>
	<?php require_once("../css/geral.css"); ?>
	<?php require_once("../css/topo_rodape.css"); ?>
	<?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



	<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13); ?>
	.bg-interna{
		background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
		background-size: 100% 12.94rem /* 207/16 */;
	}
	</style>


	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>



</head>





<body class="bg-interna">


	<?php
	$voltar_para = 'servicos'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php")
	?>


	<div class="row">
		<div class="col-12 localizacao-pagina-dica top35">
			<h3><?php echo Util::imprime($dados_dentro[titulo]) ?></h3>
		</div>
	</div>







		<div class="row">



			<div class="col-12 top30">

				<?php
				$result = $obj_site->select("tb_galerias_servicos", "AND id_servico = '$dados_dentro[0]' ");
				if(mysql_num_rows($result) == 0){ ?>

					<amp-carousel
					width="480"
					height="250"
					layout="responsive"
					type="slides"
					autoplay
					delay="4000"
					>

					<amp-img
					on="tap:lightbox2"
					role="button"
					tabindex="0"
					src="<?php echo Util::caminho_projeto() ?>/imgs/default.png"
					layout="responsive"
					width="480"
					height="250"
					alt="default">

				</amp-img>
			</amp-carousel>

			<amp-image-lightbox id="lightbox2"layout="nodisplay">	</amp-image-lightbox>

		<?php }else{?>
			<amp-carousel id="carousel-with-preview"
			width="480"
			height="250"
			layout="responsive"
			type="slides"
			autoplay
			delay="4000"
			>

			<?php
			$i = 0;
			$result = $obj_site->select("tb_galerias_servicos", "and id_servico = $dados_dentro[idservico]");
			if (mysql_num_rows($result) > 0) {
				while($row = mysql_fetch_array($result)){
					?>


					<amp-img
					on="tap:lightbox1"
					role="button"
					tabindex="0"

					src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
					layout="responsive"
					width="480"
					height="250"
					alt="<?php echo Util::imprime($row[titulo]) ?>">

				</amp-img>
				<?php
			}
			$i++;
		}


		?>

	</amp-carousel>

	<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>

	<?php /*
	<div class="carousel-preview text-center">

	<?php
	$i = 0;
	$result = $obj_site->select("tb_galerias_produtos", "and id_produto = $dados_dentro[idproduto]");
	if (mysql_num_rows($result) > 0) {
	while($row = mysql_fetch_array($result)){
	?>

	<button class="rounded-circle left5" on="tap:carousel-with-preview.goToSlide(index=<?php echo $i++; ?>)">

	<amp-img class="rounded-circle"
	src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bolinhas.png"
	width="20"
	height="20"
	alt="<?php echo Util::imprime($row[titulo]) ?>">
	</amp-img>
	</button>

	<?php
	}
	$i++;
	}


	?>
	</div>

	*/ ?>

	<?php } ?>

	</div>
	</div>
	<div class="row">




<!-- ======================================================================= -->
<!-- DESCRICAO TIPOS SERVICOS  -->
<!-- ======================================================================= -->
<div class="col-12 top20 descricao_tipo_produto">

	<!-- ======================================================================= -->
	<!-- ATENDIMENTO HOME -->
	<!-- ======================================================================= -->
	<h2>
		<a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
			<i class="fa fa-phone-square right10" aria-hidden="true"></i>
			<span class="right10">  <?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?> <br>
		</a>
		<?php if (!empty($config[telefone2])) { ?>
			<a href="tel:+55<?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>">

				<amp-img class="right10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/whats.png" height="25" width="25" alt=""></amp-img>
				<span class="right10"> <?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?>
			</a>
		</h2>
	<?php } ?>



	<!-- ======================================================================= -->
	<!-- ATENDIMENTO HOME -->
	<!-- ======================================================================= -->

	<div class="text-center top10 ">
		<a class="btn btn_empresa_home col-11" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $dados_dentro[0] ?>&tipo_orcamento=servico" class="btn btn_prod_solicitar_orcamento" title="SOLICITAR UM ORÇAMENTO">
			<i class="fa fa-shopping-cart right10" aria-hidden="true"></i>ADICIONAR AO ORÇAMENTO
		</a>
	</div>


	<!-- ======================================================================= -->
	<!-- DESCRICAO TIPOS SERVICOS  -->
	<!-- ======================================================================= -->

</div>
</div>
</div>






<div class="row">
	<div class="col-12 top10">
		<p><?php echo Util::imprime($dados_dentro[descricao]) ?></p>
	</div>
</div>






<!-- ======================================================================= -->
<!-- VEJA TAMBEM -->
<!-- ======================================================================= -->
<div class="row bottom50">

	<div class="col-12 clientes_emp">
		<h2> VEJA TAMBÉM	</h2>
	</div>



	<div class="top50 servicos_home">
		<?php
		$result = $obj_site->select("tb_servicos","ORDER BY RAND() LIMIT 2");
		if (mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_array($result)){
				//  busca a qtd de produtos cadastrados
				// $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


				switch ($i) {
					case 1:
					$cor_class = 'FACHADA_EM_LONA';
					break;
					case 2:
					$cor_class = 'CARTOES_VISITAS';
					break;
					case 3:
					$cor_class = 'BANNER_EM_CAVALE';
					break;
					case 4:
					$cor_class = 'FAIXAS_TECIDOS';
					break;
					case 5:
					$cor_class = 'EMPRESSAO_EM_TECIDOS';
					break;

					default:
					$cor_class = 'WIND_FANG';
					break;
				}
				$i++;
				?>
				<!-- ======================================================================= -->
				<!--ITEM 01 REPETI ATE 6 ITENS   -->
				<!-- ======================================================================= -->

				<div class="col-6 text-center">
					<a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
						<div class="circle">
							<amp-img  layout="responsive" class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" height="50" width="50" alt="<?php echo Util::imprime($row[titulo]) ?>"></amp-img>
							<div class="col-12  servicos_titulo text-center"><h1 ><?php Util::imprime($row[titulo]); ?></h1></div>
						</div>
					</a>
				</div>

				<?php
			}
		}
		?>


	</div>

</div>
<!-- ======================================================================= -->
<!-- VEJA TAMBEM -->
<!-- ======================================================================= -->




<?php require_once("../includes/rodape.php") ?>

</body>



</html>
