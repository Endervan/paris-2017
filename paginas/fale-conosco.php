<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 3px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12">
        <div class="top90">
          <h5>FALE CONOSCO</h5>
          <h5><span class="clearfix"> Envie suas dúvidas, sugestões, elogios ou críticas.</span></h5>


        </div>

        <div class="col-xs-7 padding0">
          <div class="media top120">
            <div class="media-left media-middle">
              <h2 class="media-object">ATENDIMENTO:</h2>
            </div>
            <div class="media-body ">
              <h2 class="media-heading top5">
                <i class="fa fa-phone-square right5" aria-hidden="true"></i>
                <span class="right5">FIXO  <?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>
                <?php if (!empty($config[telefone2])) { ?>
                  <img class="right5" src="<?php echo Util::caminho_projeto(); ?>/imgs/whats.png" alt="">
                  <span class="right5">WHATSAPP <?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?></h2>
                  <?php } ?>

                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="top15 bottom10">
              <p>Horário de Funcionamento: De 7h30 às 12h30 e das 13h30 às 18h15</p>
            </div>
          </div>
          <!-- ======================================================================= -->
          <!--TITULO GERAL -->
          <!-- ======================================================================= -->

        </div>
      </div>


      <!-- ======================================================================= -->
      <!-- fale conosco -->
      <!-- ======================================================================= -->
      <div class="container">
        <div class="row">

          <!--  ==============================================================  -->
          <!-- FORMULARIO CONTATOS-->
          <!--  ==============================================================  -->
          <div class="col-xs-8 bg_branco_produtos ">
            <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
              <div class="fundo_formulario">
                <div class="top40">
                  <h3 ><blockquote>ENVIE UM E-MAIL</blockquote></h3>
                </div>

                <!-- formulario orcamento -->
                <div class="top20">
                  <div class="col-xs-6">
                    <div class="form-group input100 has-feedback">
                      <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                      <span class="fa fa-user form-control-feedback top15"></span>
                    </div>
                  </div>

                  <div class="col-xs-6">
                    <div class="form-group  input100 has-feedback">
                      <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                      <span class="fa fa-envelope form-control-feedback top15"></span>
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>


                <div class="top20">
                  <div class="col-xs-6">
                    <div class="form-group  input100 has-feedback">
                      <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                      <span class="fa fa-phone form-control-feedback top15"></span>
                    </div>
                  </div>

                  <div class="col-xs-6">
                    <div class="form-group  input100 has-feedback">
                      <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                      <span class="glyphicon glyphicon-star form-control-feedback top5"></span>
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>


                <div class="top15">
                  <div class="col-xs-12">
                    <div class="form-group input100 has-feedback">
                      <textarea name="mensagem" cols="25" rows="10" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                      <span class="fa fa-pencil form-control-feedback top15"></span>
                    </div>
                  </div>
                </div>

                <div class="col-xs-6 top30">

                </div>
                <div class="col-xs-6 text-right">
                  <div class="top15 bottom25">
                    <button type="submit" class="btn btn_formulario" name="btn_contato">
                      ENVIAR
                    </button>
                  </div>
                </div>

              </div>
            </form>
          </div>
          <!--  ==============================================================  -->
          <!-- FORMULARIO CONTATOS-->
          <!--  ==============================================================  -->


          <!-- ======================================================================= -->
          <!-- CONTATOS  -->
          <!-- ======================================================================= -->
          <div class="col-xs-4 top180">
            <div class="telefone_contatos left20">

              <!-- Nav tabs -->
              <div class="menu_contatos">
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active">

                    <a>
                      <div class="media">
                        <div class="media-left media-middle">
                          <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_contato.png" alt="">
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading">TIRE SUAS DUVIDAS <span class="clearfix">FALE CONOSCO</span></h4>
                        </div>
                      </div>
                    </a>
                  </li>

                  <li role="presentation" >
                    <a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">

                      <div class="media">
                        <div class="media-left media-middle">
                          <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_trabalhe.png" alt="">
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading">  FAÇA PARTE DA NOSSA EQUIPE <span class="clearfix">TRABALHE CONOSCO</span></h4>
                        </div>
                      </div>

                    </a>
                  </li>
                </ul>

              </div>

            </div>
            <!-- ======================================================================= -->
            <!-- CONTATOS  -->
            <!-- ======================================================================= -->

          </div>
        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- fale conosco -->
      <!-- ======================================================================= -->


      <div class='container top100'>
        <div class="row">

          <div class="col-xs-6 col-xs-offset-3">
            <div class="media left40">
              <div class="media-left media-middle">
                <img class="right10" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao.png" alt="" />

              </div>
              <div class="media-body ">
                <h3 class="media-heading">SAIBA COMO CHEGAR</h3>
              </div>
            </div>
          </div>

          <div class="col-xs-12 mapa bg_branco_produtos pt15 pb10 top35">
            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="401" frameborder="0" style="border:0" allowfullscreen></iframe>

            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->
          </div>
        </div>
      </div>

      <div class="top100"></div>


      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/rodape.php') ?>
      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->



    </body>

    </html>


    <?php require_once('./includes/js_css.php') ?>




    <?php
    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_POST[nome]))
    {
      $texto_mensagem = "
      Nome: ".($_POST[nome])." <br />
      Email: ".($_POST[email])." <br />
      Telefone: ".($_POST[telefone])." <br />
      Assunto: ".($_POST[assunto])." <br />
      Fala com: ".($_POST[fala])." <br />

      Mensagem: <br />
      ".(nl2br($_POST[mensagem]))."
      ";


      if (Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email])) {
        Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
        Util::alert_bootstrap("Obrigado por entrar em contato.");
        unset($_POST);
      }else{
        Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
      }

  }

    ?>



    <script>
    $(document).ready(function() {
      $('.FormContatos').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'fa fa-check',
          invalid: 'fa fa-remove',
          validating: 'fa fa-refresh'
        },
        fields: {
          nome: {
            validators: {
              notEmpty: {
                message: 'Insira seu nome.'
              }
            }
          },
          mensagem: {
            validators: {
              notEmpty: {
                message: 'Insira sua Mensagem.'
              }
            }
          },
          email: {
            validators: {
              notEmpty: {
                message: 'Informe um email.'
              },
              emailAddress: {
                message: 'Esse endereço de email não é válido'
              }
            }
          },
          telefone: {
            validators: {
              notEmpty: {
                message: 'Por favor informe seu numero!.'
              },
              phone: {
                country: 'BR',
                message: 'Informe um telefone válido.'
              }
            },
          },
          assunto: {
            validators: {
              notEmpty: {

              }
            }
          }
        }
      });
    });
    </script>
