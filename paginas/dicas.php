<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>


    
<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  3px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 text-center">
        <div class="top80">
          <h4>NOSSAS DICAS</h4>
        </div>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_home.png" alt="">
      </div>
      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- nossas dicas -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top20">


      <?php
      $result = $obj_site->select("tb_dicas");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          $i=0;
          ?>
          <div class="col-xs-4 top15 dicas_geral">
            <div class="thumbnail">
              <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 224, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>

              <div class="caption">
                <h1 class="dicas_desc"><?php Util::imprime($row[titulo]); ?></h1>
                <div class="desc_dicas">
                      <p><?php Util::imprime($row[descricao]); ?></p>
                </div>
                </div>
                  </a>
              </div>
            </div>
            <?php
            if ($i==2) {
              echo "<div class='clearfix'>  </div>";
            }else {
              $i++;
            }
          }
        }
        ?>


      </div>
    </div>

    <!-- ======================================================================= -->
    <!-- nossas dicas -->
    <!-- ======================================================================= -->





    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>
