<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  3px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <div class="container">
    <div class="row ">


      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-6 titulo_emp">
        <div class="top80">
          <h1>CONHEÇA MAIS O</h1>
          <h4>GRUPO PARIS</h4>
          <h3>COMUNICAÇÃO VISUAL E IMPRESSÃO DIGITAL</h3>
        </div>
      </div>
      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>





  <div class="container top60">
    <div class="row ">

      <!-- ======================================================================= -->
      <!-- conheca nossa empresa  -->
      <!-- ======================================================================= -->
      <div class="col-xs-7 top60">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 10);?>

        <div class="top35 desc_empresa">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

        </div>
        <!-- ======================================================================= -->
        <!-- conheca nossa empresa  -->
        <!-- ======================================================================= -->

        <div class="clearfix"></div>
        <div class="top40">  </div>

        <!-- ======================================================================= -->
        <!-- MISSAO  -->
        <!-- ======================================================================= -->
        <div class="col-xs-4 ">
          <div class="bg_amarelo pg10 text-center">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 12);?>
            <div class="top15">
              <h3><?php Util::imprime($row[titulo]); ?></h3>
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_home.png" alt="">
            </div>

            <div class="top25 pb40">
              <p><?php Util::imprime($row[descricao]); ?></p>
           </div>

          </div>
          </div>
          <!-- ======================================================================= -->
          <!-- MISSAO  -->
          <!-- ======================================================================= -->

          <!-- ======================================================================= -->
          <!-- VALORES  -->
          <!-- ======================================================================= -->
          <div class="col-xs-4 ">
            <div class="bg_amarelo pg10 text-center">
              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 13);?>
              <div class="top15">
                <h3><?php Util::imprime($row[titulo]); ?></h3>
                <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_home.png" alt="">
              </div>

              <div class="top25 pb40">
                <p><?php Util::imprime($row[descricao]); ?></p>
              </div>

            </div>
            </div>
            <!-- ======================================================================= -->
            <!-- VALORES  -->
            <!-- ======================================================================= -->

   
            <!-- ======================================================================= -->
            <!-- VISAO  -->
            <!-- ======================================================================= -->
            <div class="col-xs-4 ">
              <div class="bg_amarelo pg10 text-center">
                <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 14);?>
                <div class="top15">
                  <h3> <?php Util::imprime($row[titulo]); ?></h3>
                  <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_home.png" alt="">
                </div>

                <div class="top25 pb40">
                  <p><?php Util::imprime($row[descricao]); ?></p>
                </div>

              </div>
              </div>
              <!-- ======================================================================= -->
              <!-- VISAO  -->
              <!-- ======================================================================= -->

      </div>
    </div>


    <!-- ======================================================================= -->
    <!--NOSSOS CLIENTES -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row">



        <div class="col-xs-12 top50">

        <h3>NOSSOS CLIENTES</h3>

          <!-- ======================================================================= -->
          <!--SLIDER CLIENTES -->
          <!-- ======================================================================= -->
            <?php require_once('./includes/slider_clientes.php'); ?>
            <!-- ======================================================================= -->
            <!--SLIDER CLIENTES -->
            <!-- ======================================================================= -->
        </div>


      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- NOSSOS CLIENTES -->
    <!-- ======================================================================= -->







    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>
') ?>
