<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  3px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 servicos">
        <div class="top80">
          <h4><span class="clearfix">CONFIRA OS NOSSOS</span> SERVIÇOS</h4>
        </div>
      </div>
      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top120">

      <div class="col-xs-12 text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 15);?>
      </div>
      <div class="col-xs-10 col-xs-offset-1 servicos_desc top35">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>


      <div class="top50 servicos_home">
        <?php
        $result = $obj_site->select("tb_servicos");
        if (mysql_num_rows($result) > 0) {
          while($row = mysql_fetch_array($result)){
            //  busca a qtd de produtos cadastrados
            // $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


            switch ($i) {
              case 1:
              $cor_class = 'FACHADA_EM_LONA';
              break;
              case 2:
              $cor_class = 'CARTOES_VISITAS';
              break;
              case 3:
              $cor_class = 'BANNER_EM_CAVALE';
              break;
              case 4:
              $cor_class = 'FAIXAS_TECIDOS';
              break;
              case 5:
              $cor_class = 'EMPRESSAO_EM_TECIDOS';
              break;

              default:
              $cor_class = 'WIND_FANG';
              break;
            }
            $i++;
            ?>
            <!-- ======================================================================= -->
            <!--ITEM 01 REPETI ATE 6 ITENS   -->
            <!-- ======================================================================= -->

            <div class="col-xs-3">
              <div class="circle">
                <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                  <h1 ><?php Util::imprime($row[titulo]); ?></h1>
                </a>
              </div>
            </div>


            <?php
          }
        }
        ?>


      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
