<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>




  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relative">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->



  <div class="topo-site">
      <!-- ======================================================================= -->
      <!-- topo    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/topo.php') ?>
      <!-- ======================================================================= -->
      <!-- topo    -->
      <!-- ======================================================================= -->
  </div>

  <!-- ======================================================================= -->
  <!-- ATENDIMENTO HOME -->
  <!-- ======================================================================= -->
      <div class="container">
        <div class="row atendimento">

          <div class="col-xs-7 ">
            <div class="media">
              <div class="media-left media-middle">
               <h2 class="media-object">ATENDIMENTO:</h2>
              </div>
              <div class="media-body ">
                <h2 class="media-heading top5">
                  <i class="fa fa-phone-square right5" aria-hidden="true"></i>
                  <span class="right5">  <?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>
                  <?php if (!empty($config[telefone2])) { ?>
                    <img class="right5" src="<?php echo Util::caminho_projeto(); ?>/imgs/whats.png" alt="">
                    <span class="right5"> <?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?></h2>
                    <?php } ?>

              </div>
            </div>
          </div>

          <div class="clearfix">  </div>

          <div class="col-xs-12">
            <a href="<?php echo Util::caminho_projeto(); ?>/">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/formas_pagamentos.png" alt="">
            </a>
          </div>

          <div class="col-xs-5">
              <img class="top20 input100" src="<?php echo Util::caminho_projeto(); ?>/imgs/forma-de-pagamentos.jpg" alt="">
          </div>

        </div>
      </div>
  <!-- ======================================================================= -->
  <!-- ATENDIMENTO HOME -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->
      <div class="container">
        <div class="row top80">

          <div class="col-xs-12 text-center">
                <h3>NOSSOS SERVIÇOS</h3>
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_home.png" alt="">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 11);?>
            </div>
            <div class="col-xs-8 col-xs-offset-2 servicos_desc top35">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>


          <div class="top50 servicos_home">
            <?php
            $result = $obj_site->select("tb_servicos", "limit 8");
            if (mysql_num_rows($result) > 0) {
              while($row = mysql_fetch_array($result)){
                //  busca a qtd de produtos cadastrados
                // $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


                switch ($i) {
                  case 1:
                  $cor_class = 'FACHADA_EM_LONA';
                  break;
                  case 2:
                  $cor_class = 'CARTOES_VISITAS';
                  break;
                  case 3:
                  $cor_class = 'BANNER_EM_CAVALE';
                  break;
                  case 4:
                  $cor_class = 'FAIXAS_TECIDOS';
                  break;
                  case 5:
                  $cor_class = 'EMPRESSAO_EM_TECIDOS';
                  break;

                  default:
                  $cor_class = 'WIND_FANG';
                  break;
                }
                $i++;
                ?>
                <!-- ======================================================================= -->
                <!--ITEM 01 REPETI ATE 6 ITENS   -->
                <!-- ======================================================================= -->

                <div class="col-xs-3">
                  <div class="circle">
                    <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                      <h1 ><?php Util::imprime($row[titulo]); ?></h1>
                    </a>
                  </div>
              </div>


                <?php
              }
            }
            ?>


      </div>

    </div>
  </div>
<!-- ======================================================================= -->
<!-- SERVICOS HOME -->
<!-- ======================================================================= -->




<div class="container-fluid">
<div class="row bg_empresa_home">

    <!-- ======================================================================= -->
    <!-- EMPRESA HOME -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row top30">

        <div class="col-xs-6 col-xs-offset-6">
          <h3>O GRUPO PARIS</h3>
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 9);?>
          <div class="empresa_desc top55">
            <p><?php Util::imprime($row[descricao], 1000); ?></p>
          </div>

          <a class="btn btn_empresa_home" href="<?php echo Util::caminho_projeto() ?>/empresa/" title="<?php Util::imprime($row[titulo]); ?>">
            CONHEÇA MAIS O GRUPO
          </a>

        </div>

        </div>
      </div>
  <!-- ======================================================================= -->
  <!-- EMPRESA HOME -->
  <!-- ======================================================================= -->
</div>
</div>





<!-- ======================================================================= -->
<!-- nossas dicas -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top40">

    <div class="col-xs-12">
      <h3>NOSSAS DICAS</h3>
      <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_dicas.jpg" alt="">
    </div>

    <?php
    $result = $obj_site->select("tb_dicas","ORDER BY RAND() LIMIT 3");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        $i=0;
        ?>
        <div class="col-xs-4 top15 dicas_geral">
          <div class="thumbnail">
            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 224, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>

            <div class="caption">
              <h1 class="dicas_desc"><?php Util::imprime($row[titulo]); ?></h1>
              <div class="desc_dicas">
                    <p><?php Util::imprime($row[descricao]); ?></p>
              </div>
              </div>
                </a>
            </div>
          </div>
          <?php
          if ($i==2) {
            echo "<div class='clearfix'>  </div>";
          }else {
            $i++;
          }
        }
      }
      ?>


    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- nossas dicas -->
  <!-- ======================================================================= -->

<div class="bottom40">  </div>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>

<?php require_once('./includes/js_css.php') ?>
