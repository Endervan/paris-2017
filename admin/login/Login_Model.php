<?php
@ob_start();
@session_start();


class Login_Model extends Dao
{

	private $nome_tabela = "tb_logins";
	private $chave_tabela = "idlogin";



	/*	==================================================================================================================	*/
	/*	FORMULARIO SENHA	*/
	/*	==================================================================================================================	*/
	public function formulario_senha($dados)
	{
	?>
    	<div class="class-form-2">
            <ul>
                <li>
                    <p>Senha<span></span></p>
                    <input type="password" name="senha" id="senha" class="validate(required)" />
                </li>

                <li>
                    <p>Confirme a senha<span></span></p>
                    <input type="password" name="senha2" id="senha2" class="validate(match(#senha))" />
                </li>
            </ul>
        </div>
    <?php
	}



	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera_senha($id, $dados)
	{
		$dados[senha] = md5($dados[senha]);

		parent::update($this->nome_tabela, $id, $dados);

		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "ALTEROU SENHA $id", $sql, $_SESSION[login][idlogin]);

		Util::script_msg("Senha alterada com sucesso.");
		Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/lista.php");

	}



	/*	==================================================================================================================	*/
	/*	RECUPERA SENHA	*/
	/*	==================================================================================================================	*/
	public function recupera_senha($email)
	{
		$email = Util::trata_dados_formulario($email);

		//	VERIFICO SE OS DADOS SAO VALIDOS
		$sql = "SELECT * FROM $this->nome_tabela WHERE email = '$email'";
		$result = parent::executasQL($sql);



		if(mysql_num_rows($result) > 0)
		{
			$dados = mysql_fetch_array($result);
			$this->gera_senha_envia_email($dados);
			return true;
		}
		else
		{
			return false;
		}

	}



	/*	==================================================================================================================	*/
	/*	GERA UMA NOVA SENHA PARA O USUARIO	*/
	/*	==================================================================================================================	*/
	public function gera_senha_envia_email($dados)
	{
		//	GERO A SENHA
		$CaracteresAceitos = 'ABCDZYWZ0123456789';
	  	$max = strlen($CaracteresAceitos)-1;
	  	$password = null;
	  	for($i=0; $i < 8; $i++)
		{
		  	$password .= $CaracteresAceitos{mt_rand(0, $max)};
	  	}

		$senha = md5($password);

		//	ATUALIZO A SENHA NO BANCO
		$sql = "UPDATE $this->nome_tabela SET senha = '$senha' WHERE $this->chave_tabela = '$dados[idlogin]'";
		parent::executaSQL($sql);


		//	ENVIO O EMAIL PARA O USUARIO
		$texto_mensagem = "
							Caro usuário, como solicitado segua abaixo sua nova senha. <br />

							Senha: $password
							";

		Util::envia_email($dados[email], "Recuperação de senha", $texto_mensagem, $_SERVER['SERVER_NAME'], "atendimento@".$_SERVER['SERVER_NAME']);

	}



	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
		$titulo = Util::trata_dados_formulario($dados[titulo]);
		$email = Util::trata_dados_formulario($dados[email]);
		$senha = md5($dados[senha]);


		if($this->verifica($email) > 0){
			Util::script_msg("Esse email já está cadastrado.");
		}else{
			//	CADASTRA O USUARIO
	        $id = parent::insert($this->nome_tabela, $dados);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "CADASTRO DO LOGIN $nome", $sql, $_SESSION[login][idlogin]);

			Util::script_msg("Cadastro efetuado com sucesso.");
	        Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");
		}



	}



	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
		$titulo = Util::trata_dados_formulario($dados[titulo]);
		$email = Util::trata_dados_formulario($dados[email]);
		$dados[senha] = md5($dados[senha]);



		//	VERIFICO SE O GRUPO JA ESTA CADASTRADO
		if($this->verifica_altera($email, $dados[id]) == 0)
		{

			parent::update($this->nome_tabela, $dados[id], $dados);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO LOGIN $dados[id]", $sql, $_SESSION[login][idlogin]);

			return true;
		}
		else
		{
			return false;
		}
	}



	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
		if($ativo == "SIM")
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}
		else
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}

	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
		//	BUSCA OS DADOS
		$row = $this->select($id);

		$sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
		parent::executaSQL($sql);

		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}







	/*	==================================================================================================================	*/
	/*	VERIFICO SE JA POSSUI O GRUPO CADASTRADO	*/
	/*	==================================================================================================================	*/
	public function verifica($email)
	{
		$sql = "SELECT * FROM " . $this->nome_tabela. " WHERE email = '$email'";
		return mysql_num_rows(parent::executaSQL($sql));
	}




	/*	==================================================================================================================	*/
	/*	VERIFICO SE JA POSSUI O GRUPO CADASTRADO QUANDO ALTERAR	*/
	/*	==================================================================================================================	*/
	public function verifica_altera($email, $id)
	{
			$sql = "SELECT * FROM " . $this->nome_tabela. " WHERE email = '$email' AND " . $this->chave_tabela. " <> '$id'";
		return mysql_num_rows(parent::executaSQL($sql));
	}





	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
		if($id != "")
		{
			$sql = "
					SELECT
						*
					FROM
						" . $this->nome_tabela. "
					WHERE
						" . $this->chave_tabela. " = '$id'
					ORDER BY
						titulo
					";
			return mysql_fetch_array(parent::executaSQL($sql));
		}
		else
		{
			$sql = "
				SELECT
					*
				FROM
					" . $this->nome_tabela. "
				ORDER BY
					titulo
				";
			return parent::executaSQL($sql);
		}

	}






	/*	==================================================================================================================	*/
	/*	EFETUA LOGIN	*/
	/*	==================================================================================================================	*/
	public function efetuar_login($email, $senha)
	{
		$email = Util::trata_dados_formulario($email);
		$senha = md5($senha);

		$sql = "SELECT * FROM tb_logins WHERE email = '$email' AND senha = '$senha' AND ativo = 'SIM'";
		$result = parent::executaSQL($sql);


		if(mysql_num_rows($result) > 0)
		{

			$row = mysql_fetch_array($result);
			$_SESSION[login] = $row;

			return true;

		}
		else
		{
			return false;
		}
	}





	public function verifica_grupo_ativo($id_grupologin)
	{
		$sql = "SELECT * FROM tb_grupos_logins WHERE idgrupologin = '$id_grupologin'";
		$row = mysql_fetch_array(parent::executaSQL($sql));

		if($row[ativo] == 'SIM')
		{
			return true;
		}
		else
		{
			return false;
		}
	}



	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
	?>

		<div class="col-xs-12 form-group ">
            <label>Nome</label>
            <input type="text" name="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="form-control fundo-form1 input100" >
        </div>

        <div class="col-xs-12 form-group ">
            <label>Email</label>
            <input type="text" name="email" value="<?php Util::imprime($dados[email]) ?>" class="form-control fundo-form1 input100" >
        </div>

        <div class="col-xs-6 form-group ">
            <label>Senha</label>
            <input type="password" name="senha"  class="form-control fundo-form1 input100" >
        </div>

        <div class="col-xs-6 form-group ">
            <label>Confirme a senha</label>
            <input type="password" name="senha2"  class="form-control fundo-form1 input100" >
        </div>



		<?php if($_SESSION[login][acesso_tags] == 'SIM'): ?>
        <div class="col-xs-12 form-group ">
            <label>Acesso ao SEO <span>Visível apenas a usuários habilitados</span></label>

            <select name="acesso_tags" class="form-control fundo-form1 input100">
            	<option value="">Selecione</option>
            	<option value="SIM" <?php if($dados[acesso_tags] == "SIM"){ echo 'selected'; } ?> >SIM</option>
            	<option value="NAO" <?php if($dados[acesso_tags] == "NAO"){ echo 'selected'; } ?> >NÃO</option>
            </select>

        </div>
        <?php endif; ?>



        <!-- ======================================================================= -->
        <!-- VALIDACAO DO FORMULARIO    -->
        <!-- ======================================================================= -->
        <script>
          $(document).ready(function() {
            $('.FormPrincipal').bootstrapValidator({
              message: 'This value is not valid',
              feedbackIcons: {
								valid: 'fa fa-check',
								invalid: 'fa fa-remove',
								validating: 'fa fa-refresh'
              },
              fields: {


              titulo: {
                validators: {
                  notEmpty: {

                  }
                }
              },


              acesso_tags: {
                validators: {
                  notEmpty: {

                  }
                }
              },




              email: {
                validators: {
                  notEmpty: {

                  },
                  emailAddress: {
                    message: 'Esse endereço de email não é válido'
                  }
                }
              },


	            senha: {
		            validators: {
		                identical: {
		                    field: 'senha2',
		                    message: 'Confirme a senha'
		                }
		            }
		        },
		        senha2: {
		            validators: {
		                identical: {
		                    field: 'senha',
		                    message: 'As senhas devem ser identicas.'
		                }
		            }
		        },


              mensagem: {
                validators: {
                  notEmpty: {

                  }
                }
              }



            }
          });
          });
        </script>

    <?php
	}







}

?>
