import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "body": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "img": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "p": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "h1": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "h2": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "h3": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "h4": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "h5": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "h6": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "ul": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "ol": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "li": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "border": "none",
        "outline": "none"
    },
    "*": {
        "marginTop": 0,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0,
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "textDecoration": "none"
    },
    "select": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "input": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "top5": {
        "marginTop": 5
    },
    "top10": {
        "marginTop": 10
    },
    "top15": {
        "marginTop": 15
    },
    "top20": {
        "marginTop": 20
    },
    "top25": {
        "marginTop": "25px !important"
    },
    "top30": {
        "marginTop": "30px !important"
    },
    "top35": {
        "marginTop": "35px !important"
    },
    "top40": {
        "marginTop": "40px !important"
    },
    "top45": {
        "marginTop": 45
    },
    "top50": {
        "marginTop": 50
    },
    "top55": {
        "marginTop": 55
    },
    "top60": {
        "marginTop": 60
    },
    "top65": {
        "marginTop": 65
    },
    "top70": {
        "marginTop": 70
    },
    "top75": {
        "marginTop": 75
    },
    "top80": {
        "marginTop": 80
    },
    "top85": {
        "marginTop": 85
    },
    "top90": {
        "marginTop": 90
    },
    "top100": {
        "marginTop": 100
    },
    "top105": {
        "marginTop": 105
    },
    "top115": {
        "marginTop": 115
    },
    "top120": {
        "marginTop": 120
    },
    "top150": {
        "marginTop": 150
    },
    "top170": {
        "marginTop": 170
    },
    "top220": {
        "marginTop": 220
    },
    "top195": {
        "marginTop": 195
    },
    "top200": {
        "marginTop": 200
    },
    "top210": {
        "marginTop": 210
    },
    "top240": {
        "marginTop": 240
    },
    "top250": {
        "marginTop": 250
    },
    "top270": {
        "marginTop": 270
    },
    "top350": {
        "marginTop": 350
    },
    "top395": {
        "marginTop": 395
    },
    "bottom5": {
        "marginBottom": 5
    },
    "bottom10": {
        "marginBottom": 10
    },
    "bottom15": {
        "marginBottom": 15
    },
    "bottom20": {
        "marginBottom": 20
    },
    "bottom25": {
        "marginBottom": 25
    },
    "bottom30": {
        "marginBottom": 30
    },
    "bottom35": {
        "marginBottom": 35
    },
    "bottom40": {
        "marginBottom": 40
    },
    "bottom45": {
        "marginBottom": 45
    },
    "bottom50": {
        "marginBottom": 50
    },
    "bottom55": {
        "marginBottom": 55
    },
    "bottom60": {
        "marginBottom": 60
    },
    "bottom65": {
        "marginBottom": 65
    },
    "bottom70": {
        "marginBottom": 70
    },
    "bottom75": {
        "marginBottom": 75
    },
    "bottom80": {
        "marginBottom": 80
    },
    "bottom90": {
        "marginBottom": 90
    },
    "bottom100": {
        "marginBottom": 100
    },
    "bottom120": {
        "marginBottom": 120
    },
    "bottom135": {
        "marginBottom": 135
    },
    "bottom180": {
        "marginBottom": 180
    },
    "bottom200": {
        "marginBottom": 200
    },
    "bottom280": {
        "marginBottom": 280
    },
    "left5": {
        "marginLeft": 5
    },
    "left10": {
        "marginLeft": 10
    },
    "left15": {
        "marginLeft": 15
    },
    "left20": {
        "marginLeft": 20
    },
    "left25": {
        "marginLeft": 25
    },
    "left30": {
        "marginLeft": 30
    },
    "left35": {
        "marginLeft": 35
    },
    "left40": {
        "marginLeft": 40
    },
    "left45": {
        "marginLeft": 45
    },
    "left50": {
        "marginLeft": 50
    },
    "left55": {
        "marginLeft": 55
    },
    "left60": {
        "marginLeft": 60
    },
    "left65": {
        "marginLeft": 65
    },
    "left70": {
        "marginLeft": 70
    },
    "left75": {
        "marginLeft": 75
    },
    "left80": {
        "marginLeft": 80
    },
    "left81": {
        "marginLeft": 81
    },
    "left130": {
        "marginLeft": 130
    },
    "left270": {
        "marginLeft": 270
    },
    "right5": {
        "marginRight": 5
    },
    "right10": {
        "marginRight": 10
    },
    "right15": {
        "marginRight": 15
    },
    "right20": {
        "marginRight": 20
    },
    "right22": {
        "marginRight": 22
    },
    "right25": {
        "marginRight": 25
    },
    "right30": {
        "marginRight": 30
    },
    "right35": {
        "marginRight": 35
    },
    "right40": {
        "marginRight": 40
    },
    "right45": {
        "marginRight": 45
    },
    "right50": {
        "marginRight": 50
    },
    "right55": {
        "marginRight": 55
    },
    "right60": {
        "marginRight": 60
    },
    "right65": {
        "marginRight": 65
    },
    "right70": {
        "marginRight": 70
    },
    "right75": {
        "marginRight": 75
    },
    "right80": {
        "marginRight": 80
    },
    "right90": {
        "marginRight": 90
    },
    "right100": {
        "marginRight": 100
    },
    "right250": {
        "marginRight": 250
    },
    "right130": {
        "marginRight": 130
    },
    "pt10": {
        "paddingTop": 10
    },
    "pt15": {
        "paddingTop": 15
    },
    "pt20": {
        "paddingTop": 20
    },
    "pt30": {
        "paddingTop": 30
    },
    "pt40": {
        "paddingTop": 40
    },
    "pt45": {
        "paddingTop": 45
    },
    "pt50": {
        "paddingTop": 50
    },
    "pt60": {
        "paddingTop": 60
    },
    "pt80": {
        "paddingTop": 80
    },
    "pt100": {
        "paddingTop": 100
    },
    "pt300": {
        "paddingTop": 300
    },
    "pl15": {
        "paddingLeft": 15
    },
    "pl20": {
        "paddingLeft": 20
    },
    "pl30": {
        "paddingLeft": 30
    },
    "pl40": {
        "paddingLeft": 40
    },
    "pl45": {
        "paddingLeft": 45
    },
    "pl50": {
        "paddingLeft": 50
    },
    "pl60": {
        "paddingLeft": 60
    },
    "pl80": {
        "paddingLeft": 80
    },
    "pl100": {
        "paddingLeft": 100
    },
    "pl300": {
        "paddingLeft": 300
    },
    "pr15": {
        "paddingRight": 15
    },
    "pr20": {
        "paddingRight": 20
    },
    "pr30": {
        "paddingRight": 30
    },
    "pr40": {
        "paddingRight": 40
    },
    "pr45": {
        "paddingRight": 45
    },
    "pr50": {
        "paddingRight": 50
    },
    "pr60": {
        "paddingRight": 60
    },
    "pr80": {
        "paddingRight": 80
    },
    "pr100": {
        "paddingRight": 100
    },
    "pr300": {
        "paddingRight": 300
    },
    "pb10": {
        "paddingBottom": 10
    },
    "pb15": {
        "paddingBottom": 15
    },
    "pb20": {
        "paddingBottom": 20
    },
    "pb30": {
        "paddingBottom": 30
    },
    "pb40": {
        "paddingBottom": "40px !important"
    },
    "pb50": {
        "paddingBottom": 50
    },
    "pb60": {
        "paddingBottom": 60
    },
    "pb80": {
        "paddingBottom": 80
    },
    "pb100": {
        "paddingBottom": 100
    },
    "pb200": {
        "paddingBottom": 200
    },
    "pb280": {
        "paddingBottom": 280
    },
    "pb300": {
        "paddingBottom": 300
    },
    "pg0": {
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0
    },
    "bg-login": {
        "background": "#FFF url(../imgs/bg-login.jpg) top center no-repeat"
    },
    "input100": {
        "width": "100% !important"
    },
    "div_personalizado": {
        "paddingTop": "0px !important",
        "paddingBottom": "0px !important"
    },
    "whatsap": {
        "fontSize": 20
    }
});